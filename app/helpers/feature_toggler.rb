require 'unleash'
require 'unleash/context'

class FeatureToggler
  def initialize
    if ENV['FEATURES_FLAGS_URL']
      @unleash = Unleash::Client.new({
                                       url: ENV['FEATURES_FLAGS_URL'],
                                       app_name: ENV['FEATURES_FLAGS_APP'],
                                       instance_id: ENV['FEATURES_FLAGS_ID']
                                     })
    end
  end

  def is_enabled(feature_name)
    if RACK_ENV['production']
      unleash_context = Unleash::Context.new
      @unleash&.is_enabled?(feature_name, unleash_context)
    else
      ENV[feature_name]
    end
  end
end
